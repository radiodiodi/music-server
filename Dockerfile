FROM node:12-alpine

WORKDIR /app
EXPOSE 7070

COPY package.json yarn.lock ./
RUN yarn
COPY src src
ENTRYPOINT ["yarn", "prod"]
